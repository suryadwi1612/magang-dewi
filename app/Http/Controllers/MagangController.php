<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MagangController extends Controller
{
    public function landing()
    {
        return view('welcome');
    }
    
    public function coba()
    {
        return view('testing');
    }
}
