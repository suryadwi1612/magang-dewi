<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        
        {{-- to use tailwind --}}
        <link rel="stylesheet" href="/css/app.css">
	</head>
	<body>
		<h1 class="text-red-500">Halaman Awal</h1>
		
		<button class="px-2 py-4 bg-pink-500 hover:bg-blue-800 text-white rounded-lg ml-10" > 
		Tombol
		</button>
		
	</body>
</html>