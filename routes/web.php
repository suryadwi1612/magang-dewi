<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/pertama', function () {
//     return view('pertama');
// });

Auth::routes();

route::get('/home', 'HomeController@index')->name('home');
route::get('/welcome', 'MagangController@landing');
route::get('/coba', 'MagangController@coba');
